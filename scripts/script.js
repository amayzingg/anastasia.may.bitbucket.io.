// * * * * SVG BURGER
const toppingButtons = document.querySelector(".toppings");
const seedButton = document.querySelector("#seeds");
const lettuceButton = document.querySelector("#lettuce");
const ketchupButton = document.querySelector("#ketchup");
const cheeseButton = document.querySelector("#cheese");

const seeds = document.querySelector(".seeds");
const lettuce = document.querySelector(".lettuce");
const ketchup = document.querySelector(".ketchup");
const cheese = document.querySelector(".cheese");

seedButton.addEventListener("click", (e) => {
  seeds.classList.toggle("removed");
});

lettuceButton.addEventListener("click", (e) => {
  lettuce.classList.toggle("removed");
});

cheeseButton.addEventListener("click", (e) => {
  cheese.classList.toggle("removed");
});

ketchupButton.addEventListener("click", (e) => {
  ketchup.classList.toggle("removed");
});

// * * * * Motion Path Plugin (new in GSAP 3)
// gsap.registerPlugin(MotionPathPlugin);

// gsap.to("#rect", {duration: 5});
// gsap.to("#divMotion", {
//     motionPath: {
//         path: "#path",
//         align: "#path",
//         alignOrigin: [0.5, 0.5],
//         autoRotate: true
//     },
//     transformOrigin: "50% 50%",
//     duration: 5,
//     ease: "power1.inOut"
// });
